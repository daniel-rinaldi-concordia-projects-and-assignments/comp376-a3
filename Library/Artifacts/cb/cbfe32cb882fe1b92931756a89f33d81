                           "č               0.0.0 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙   Ŕ           1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               \     ˙˙˙˙               H r   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                     Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                        \       ŕyŻ     `                                                                                                                                                       ŕyŻ         Î˙                                                                         MBObjectPooler    using UnityEngine;

using System;
using System.Collections.Generic;

using Design_Patterns;

namespace Game.Objects
{
    /// <summary>
    /// A monobehaviour object pool.
    /// </summary>
    public class MBObjectPooler : MBSingleton<MBObjectPooler>
    {
        [System.Serializable]
        public class PoolDescriptor
        {
            public string name;
            public GameObject prefab;
            public int size;
        }

        [Tooltip("The default parent object for spawned objects. If null, then objects will be spawned as a child of this object.")]
        [SerializeField] protected GameObject defaultSpawnParent;

        [SerializeField] protected PoolDescriptor[] poolDescriptors;
        // This Dictionary's values are Tuples that contain the following: (Item1:"object pool queue", "index of PoolDescriptor related to the objects in Item1's queue")
        protected Dictionary<string, Tuple<Queue<GameObject>, int>> pool_dictionary;

        protected override void Awake()
        {
            base.Awake();

            pool_dictionary = new Dictionary<string, Tuple<Queue<GameObject>, int>>();
            
            for (int i = 0; i < poolDescriptors.Length; i++)
            {
                if (!pool_dictionary.ContainsKey(poolDescriptors[i].name))
                {
                    Tuple<Queue<GameObject>, int> object_pool_tuple = new Tuple<Queue<GameObject>, int>(new Queue<GameObject>(), i);
                    for (int j = 0; j < poolDescriptors[i].size; j++)
                    {
                        GameObject obj = Instantiate(poolDescriptors[i].prefab, this.gameObject.transform);
                        obj.SetActive(false);
                        object_pool_tuple.Item1.Enqueue(obj);
                    }

                    pool_dictionary.Add(poolDescriptors[i].name, object_pool_tuple);
                }
            }
        }

        public void ResizePool(string pool_name, int size)
        {
            if (!pool_dictionary.ContainsKey(pool_name))
            {
                Debug.LogError("Pool with name '" + pool_name + "' doesn't exist.");
                return;
            }

            Tuple<Queue<GameObject>, int> object_pool_tuple = pool_dictionary[pool_name];

            if (poolDescriptors[object_pool_tuple.Item2].size == size) return;

            if (size <= object_pool_tuple.Item1.Count)
            {
                for (int i = 0; i < size; i++)
                    Destroy(object_pool_tuple.Item1.Dequeue());
            }
            else
            {
                int extra = size - object_pool_tuple.Item1.Count;
                for (int i = 0; i < extra; i++)
                {
                    GameObject obj = Instantiate(poolDescriptors[object_pool_tuple.Item2].prefab);
                    obj.SetActive(false);
                    object_pool_tuple.Item1.Enqueue(obj);
                }
            }
            poolDescriptors[object_pool_tuple.Item2].size = size;
        }

        public GameObject Spawn(string pool_name, Vector3 position, Quaternion rotation, GameObject parent = null)
        {
            if (rotation == null) rotation = Quaternion.identity;
            if (parent == null) parent = defaultSpawnParent;
            if (parent == null) parent = this.gameObject;

            if (!pool_dictionary.ContainsKey(pool_name))
            {
                Debug.LogError("Pool with name '" + pool_name + "' doesn't exist.");
                return null;
            }

            GameObject obj = pool_dictionary[pool_name].Item1.Dequeue();
            obj.transform.parent = parent.transform;
            obj.SetActive(true);
            obj.transform.position = parent.transform.position + position;
            obj.transform.rotation = rotation;

            IPooledObject pooled_obj = obj.GetComponent<IPooledObject>();
            if (pooled_obj != null)
                pooled_obj.OnSpawn();

            pool_dictionary[pool_name].Item1.Enqueue(obj);

            return obj;
        }

        public void Despawn(GameObject game_object)
        {
            game_object.SetActive(false);
        }
    }

    /// <summary>
    /// An interface for pooled objects.
    /// </summary>
    public interface IPooledObject
    {
        /// <summary>
        /// This function is called when Object is spawned via ObjectPooler.Spawn()
        /// See <see cref="MBObjectPooler.Spawn()"/>
        /// </summary>
        void OnSpawn();
    }
}                Î˙˙˙   MBObjectPooler     Game.Objects