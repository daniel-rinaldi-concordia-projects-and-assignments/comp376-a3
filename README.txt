Project was built using Unity Version 2019.2.5f1
In order for the game to run correctly, Open only the "Splash" Scene in the editor and hit play.

There are two modes that can be selected in the main menu:
- Play : Normal version of the game with no power up.
- Play Special : Special version includes a speed boost every once in a while.

How to play:
- wasd controls character movement
- spacebar jumps above ground and swims upwards underwater
- Alpha 1 (the number above the alphabetical keys on the keyboard) will activate the speed boost given that you have it and are in that game mode.

*********************************************************************************
*********************************************************************************
**                                                                             **
** LINK TO GAMEPLAY VIDEO : https://www.youtube.com/watch?v=JVKjHNk1ISU        **
**                                                                             **
*********************************************************************************
*********************************************************************************

Other things to note:
I did not add a pause feature or a way to go back to the main menu mid game, 
so if you want to go back to the main menu you will have to die.

Cheers :)
