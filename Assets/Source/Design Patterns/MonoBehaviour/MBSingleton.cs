﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Design_Patterns
{
    /// <summary>
    /// Inherit from this base class to create a monobehaviour singleton.
    /// e.g. public class MyClassName : MBSingleton<MyClassName> {...}
    /// </summary>
    public abstract class MBSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {

        private static bool destroyed = false;
        private static object mutex = new object();
        private static T instance;

        [SerializeField] private bool destroyOnLoad;

        public static T Instance
        {
            get
            {
                if (destroyed)
                {
                    Debug.LogWarning("[MBSingleton] Instance '" + typeof(T) + "' has already been destroyed. Creating new instance.");
                    destroyed = false;
                }

                return _CreateInstance(null);
            }
        }

        private static T _CreateInstance(GameObject game_object)
        {
            lock (mutex)
            {
                // Create new instance if one doesn't already exist
                if (instance == null)
                {
                    if (game_object == null)
                    {
                        // create the GameObject that will house the singleton instance
                        game_object = new GameObject(typeof(T).Name);
                        game_object.AddComponent<T>();
                    }

                    instance = game_object.GetComponent<T>();
                }

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
                _CreateInstance(this.gameObject);
            else
            {
                Destroy(this.gameObject);
                destroyed = false;
                return;
            }

            if (!DestroyOnLoad)
                DontDestroyOnLoad(this.gameObject);
        }

        protected virtual void OnApplicationQuit()
        {
            Destroy(this.gameObject);
        }

        protected virtual void OnDestroy()
        {
            instance = null;
            destroyed = true;
        }

        public bool DestroyOnLoad
        {
            get { return destroyOnLoad; }
            set
            {
                destroyOnLoad = value;
                if (destroyOnLoad)
                    SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetActiveScene());
                else
                    DontDestroyOnLoad(this.gameObject);
            }
        }
    }
}