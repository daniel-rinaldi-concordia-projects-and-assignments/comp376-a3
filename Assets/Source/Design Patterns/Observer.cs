﻿
namespace Design_Patterns
{
    public interface Observer
    {
        void Notify(Subject subject);
    }

    public interface Subject
    {
        void RegisterObserver(Observer observer);
        void UnregisterObserver(Observer observer);
        void NotifyObservers();
    }
}
