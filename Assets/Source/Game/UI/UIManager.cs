﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

using Design_Patterns;

using UnityEngine;

namespace Game.UI
{
    public class UIManager : MBSingleton<UIManager>
    {
        [Tooltip("List of UILayer prefabs")]
        [SerializeField] private List<GameObject> uiLayerPrefabList;

        private Stack<UILayer> uilayer_stack;

        protected override void Awake()
        {
            base.Awake();
            uilayer_stack = new Stack<UILayer>();
            if (uiLayerPrefabList == null) uiLayerPrefabList = new List<GameObject>();
        }

        public T CreateUILayer<T>() where T : UILayer
        {
            T uilayer_prefab = GetPrefab<T>();
            T uilayer = Instantiate(uilayer_prefab, this.transform);
            return uilayer;
        }
        private T GetPrefab<T>() where T : UILayer
        {
            foreach (GameObject uilayer_prefab in uiLayerPrefabList)
            {
                if (uilayer_prefab != null && uilayer_prefab.GetComponent<T>() != null)
                    return uilayer_prefab.GetComponent<T>();
            }

            throw new MissingReferenceException("[UIManager] Cannot find prefab script for type : " + typeof(T));
        }

        public T OpenUI<T>() where T : UILayer
        {
            return (T)OpenUI(CreateUILayer<T>());
        }

        public UILayer OpenUI(UILayer uilayer)
        {
            if (uilayer_stack.Count > 0)
            {
                foreach (UILayer uil in uilayer_stack)
                {
                    uil.DisableUI();

                    if (uilayer.hideWindowsUnderneath)
                        uil.Hide();

                    if (uil.hideWindowsUnderneath) break;
                }

                // make sure new window is on top of previous in the Scene
                Canvas top = uilayer.GetComponent<Canvas>();
                Canvas previous = uilayer_stack.Peek().GetComponent<Canvas>();
                top.sortingOrder = previous.sortingOrder + 1;
            }

            uilayer.gameObject.name = uilayer.gameObject.name + " [" + (uilayer_stack.Count).ToString() + "]";
            uilayer_stack.Push(uilayer);
            uilayer.gameObject.SetActive(true);
            uilayer.Open();

            return uilayer_stack.Peek();
        }

        public void CloseTopUILayer()
        {
            if (uilayer_stack.Count == 0) return;

            UILayer uilayer = uilayer_stack.Pop();

            if (uilayer.destroyWhenClosed)
            {
                uilayer.Close();
                Destroy(uilayer.gameObject);
            }
            else
            {
                uilayer.gameObject.SetActive(false);
                uilayer.Close();
                uilayer.gameObject.name = Regex.Replace(uilayer.gameObject.name, @"\[\d+\](?!.*\[\d+\])", "");
            }

            foreach (UILayer uil in uilayer_stack)
            {
                uil.Show();
                if (uil.hideWindowsUnderneath) break;
            }

            if (uilayer_stack.Count > 0)
                uilayer_stack.Peek().Focus();
        }

        public void CloseAllUILayers()
        {
            do CloseTopUILayer();
            while (uilayer_stack.Count > 0);
        }

        public UILayer top_layer
        {
            get { return (uilayer_stack != null || uilayer_stack.Count > 0) ? uilayer_stack.Peek() : null; }
        }
    }
}