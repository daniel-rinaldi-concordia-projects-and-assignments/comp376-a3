﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.UI
{
    public abstract class UILayer : MonoBehaviour
    {
        [Tooltip("Hide windows that are under this one in the UI stack")]
        [SerializeField] public bool hideWindowsUnderneath = true;

        [Tooltip("Destroy the GameObject when UI is closed (saves memory, if false, then you have to manually manage this instance after closing it)")]
        [SerializeField] public bool destroyWhenClosed = true;

        [Tooltip("The game object to focus on when UI is focused")]
        [SerializeField] public GameObject focusTarget;

        private bool ui_enabled;

        protected virtual void OnOpen() { }
        protected virtual void OnClose() { }
        protected virtual void OnShow() { }
        protected virtual void OnHide() { }
        protected virtual void OnEnableUI() { }
        protected virtual void OnDisableUI() { }
        protected virtual void OnFocus() { }
        protected virtual void OnUpdate() { }

        protected virtual void Awake()
        {
            ui_enabled = true;
            this.gameObject.AddComponent<CanvasGroup>();
        }

        public void Open()
        {
            Focus();
            OnOpen();
        }

        public void Close()
        {
            DisableUI();
            OnClose();
        }

        public void Show()
        {
            this.gameObject.SetActive(true);
            OnShow();
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
            OnHide();
        }

        public void EnableUI()
        {
            this.GetComponent<CanvasGroup>().interactable = ui_enabled = true;
            OnEnableUI();
        }

        public void DisableUI()
        {
            this.GetComponent<CanvasGroup>().interactable = ui_enabled = false;
            OnDisableUI();
        }

        public void Focus()
        {
            EnableUI();
            EventSystem.current.SetSelectedGameObject(focusTarget);
            OnFocus();
        }

        private void Update()
        {
            if (ui_enabled) OnUpdate();
        }

        public bool isEnabled()
        {
            return ui_enabled;
        }
    }
}
