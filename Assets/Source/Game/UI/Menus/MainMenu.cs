﻿using UnityEngine;

using Game.Audio;
using Game.UI.Dialogs;
using Game.Scenes;

namespace Game.UI.Menus
{
    public class MainMenu : UILayer
    {
        public void NewGameClick(int gameMode)
        {
            GameManager.Instance.GameMode = (GameManager.Mode) gameMode;
            SceneLoader.Instance.LoadScene("Gameplay", SceneLoader.TransitionType.CIRCLE, () => 
            {
                UIManager.Instance.CloseAllUILayers();
            });
        }

        public void CreditsClick()
        {
            SceneLoader.Instance.LoadScene("Credits", SceneLoader.TransitionType.FADE, () =>
            {
                UIManager.Instance.CloseAllUILayers();
            });
        }

        public void QuitClick()
        {
            ConfirmDialog cw = UIManager.Instance.CreateUILayer<ConfirmDialog>();
            cw.LinkData("Are you sure you want to quit?", (bool result, ConfirmDialog dialog) =>
            {
                if (result)
                {
                    UIManager.Instance.CloseAllUILayers();
                    AudioManager.Instance.Stop();
                    Application.Quit();
                }
                else
                    UIManager.Instance.CloseTopUILayer();
            });
            UIManager.Instance.OpenUI(cw);
        }
    }
}
