﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Dialogs
{
    public class ConfirmDialog : UILayer
    {
        [SerializeField] public Text txtMessage;
        [SerializeField] public Text txtConfirm;
        [SerializeField] public Text txtCancel;

        public const float DEFAULT_WIDTH = 600f;

        public delegate void Callback(bool result, ConfirmDialog self);
        private Callback callback;

        public void LinkData(string message, Callback callback, float width = DEFAULT_WIDTH, string str_confirm = null, string str_cancel = null)
        {
            if (str_confirm == null) str_confirm = "Confirm";
            if (str_cancel == null) str_cancel = "Cancel";

            txtMessage.text = message;
            this.callback = callback != null ? callback : (bool r, ConfirmDialog s) => { };
            txtConfirm.text = str_confirm;
            txtCancel.text = str_cancel;
        }

        public void ConfirmClick()
        {
            callback(true, this);
        }

        public void CancelClick()
        {
            callback(false, this);
        }
    }
}
