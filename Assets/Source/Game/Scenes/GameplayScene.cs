﻿using UnityEngine;
using UnityEngine.UI;

using Game.Audio;
using Game.Objects;

using Design_Patterns;

namespace Game.Scenes
{
    public class GameplayScene : MonoBehaviour, Observer
    {
        [SerializeField] private float levelChangeTime;
        private Timer level_change_timer;

        [SerializeField] private Text levelText;

        private void Awake()
        {
            AudioManager.Instance.PlayMusic("Into the Unknown", new AudioManager.MusicOptions(AudioManager.MusicTransition.FADE));

            level_change_timer = new Timer("LevelChangeTimer", levelChangeTime, true, true);
            level_change_timer.RegisterObserver(this);
            GameManager.Instance.CurrentLevel = 1;
            levelText.text = GameManager.Instance.CurrentLevel.ToString();
            GameManager.Instance.Score = 0;
        }

        public void Update()
        {
            level_change_timer.UpdateTimer(Time.deltaTime);
        }

        public void Notify(Subject subject)
        {
            if (subject is Timer && ((Timer)subject).Name == "LevelChangeTimer")
            {
                GameManager.Instance.CurrentLevel++;
                levelText.text = GameManager.Instance.CurrentLevel.ToString();
            }
        }
    }
}