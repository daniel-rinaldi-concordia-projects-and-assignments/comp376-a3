﻿using UnityEngine;

using Game.Audio;

namespace Game.Scenes
{
    public class SplashScene : MonoBehaviour
    {
        [SerializeField] private string nextScene = "";

        private void Awake()
        {
            AudioManager.Instance.PlayMusic("Salutations");
        }

        public void OnSplashComplete()
        {
            SceneLoader.Instance.LoadScene(nextScene, SceneLoader.TransitionType.FADE);
        }
    }
}