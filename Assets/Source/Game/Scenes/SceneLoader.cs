﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

using Design_Patterns;

namespace Game.Scenes
{
    public class SceneLoader : MBSingleton<SceneLoader>
    {
        [SerializeField] private GameObject transitionsObject;

        /// <summary>
        /// This is set when LoadingScene is loaded. See <see cref="LoadingScene.Awake()" />
        /// </summary>
        [HideInInspector] public RectTransform loading_bar;

        public enum TransitionType
        {
            NONE, // <-- must be first
            FADE, CIRCLE, CIRCULAR_FAN, BOX, EYELID, DIAMOND, HORIZONTAL_SLIDE, VERTICAL_SLIDE, DIAGONAL_BLINDS, HORIZONTAL_BLINDS, VERTICAL_BLINDS,
            RANDOM // <-- must be last
        }
        public enum TransitionPhase { NONE, OUT, IN }

        public delegate void OnBeforeSceneLoadCallback();

        private Animator animator;
        private string scene_to_load;
        private OnBeforeSceneLoadCallback before_scene_load_callback;
        private TransitionType _transition_type;
        private TransitionPhase _transition_phase;

        protected override void Awake()
        {
            base.Awake();
            animator = this.GetComponent<Animator>();
            transitionsObject.SetActive(false);
        }

        public void LoadScene(string scene_name, TransitionType type = TransitionType.NONE, OnBeforeSceneLoadCallback on_before_scene_load_callback = null)
        {
            scene_to_load = scene_name;
            before_scene_load_callback = on_before_scene_load_callback;

            transitionsObject.SetActive(true);
            transition_type = type;
            transition_phase = TransitionPhase.OUT;

            if (_transition_type == TransitionType.NONE)
            {
                OnOutComplete();
                OnInComplete();
            }
        }

        public void OnOutComplete()
        {
            if (!string.IsNullOrWhiteSpace(scene_to_load))
            {
                before_scene_load_callback?.Invoke();
                SceneManager.LoadScene("Loading");
                StartCoroutine(LoadSceneCoroutine(scene_to_load));
            }
            else
            {
                before_scene_load_callback?.Invoke();
                transition_phase = TransitionPhase.IN;
            }

            scene_to_load = null;
            before_scene_load_callback = null;
        }

        public void OnInComplete()
        {
            transitionsObject.SetActive(false);
            transition_type = TransitionType.NONE;
            transition_phase = TransitionPhase.NONE;
        }

        public TransitionType transition_type
        {
            get { return _transition_type; }
            set
            {
                _transition_type = value;
                if (value != TransitionType.RANDOM)
                    animator?.SetInteger("TransitionType", (int)value);
                else
                    animator?.SetInteger("TransitionType", Random.Range((int)TransitionType.NONE, (int)TransitionType.RANDOM));
            }
        }

        public TransitionPhase transition_phase
        {
            get { return _transition_phase; }
            set
            {
                _transition_phase = value;
                animator?.SetInteger("TransitionPhase", (int)value);
            }
        }

        private IEnumerator LoadSceneCoroutine(string scene_name)
        {
            yield return null; // continue running on the next frame

            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene_name);
            asyncOperation.allowSceneActivation = false;

            RectTransform progress_transform = ((RectTransform)loading_bar.GetChild(0));
            progress_transform.sizeDelta = new Vector2(0, progress_transform.sizeDelta.y);

            while (!asyncOperation.isDone)
            {
                float bar_length = loading_bar.rect.width;
                float progress = (asyncOperation.progress + 0.1f); // we add 0.1 because it stops at 0.9 when asyncOperation.allowSceneActivation is false

                if (progress_transform != null)
                    progress_transform.sizeDelta = new Vector2(progress * bar_length, progress_transform.sizeDelta.y);

                if (progress >= 1f)
                    asyncOperation.allowSceneActivation = true; // activate the new scene

                yield return null;
            }

            loading_bar = null;
            transition_phase = TransitionPhase.IN;
        }
    }
}