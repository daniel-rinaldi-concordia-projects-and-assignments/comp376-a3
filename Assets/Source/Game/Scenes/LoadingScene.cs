﻿using UnityEngine;

namespace Game.Scenes
{
    public class LoadingScene : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private RectTransform loadingBar;

        [Tooltip("Time (in frames) to wait until Canvas is activated")]
        [SerializeField] private float canvasShowTime = 0;

        private float canvas_timer;

        private void Awake()
        {
            SceneLoader.Instance.loading_bar = loadingBar;
            canvas.gameObject.SetActive(false);
            canvas_timer = 0;
        }

        private void Update()
        {
            canvas_timer += Time.deltaTime;
            if (canvas_timer >= canvasShowTime)
                canvas.gameObject.SetActive(true);
        }
    }
}
