﻿using UnityEngine;

namespace Game.Scenes
{
    public class CreditsScene : MonoBehaviour
    {
        [SerializeField] private RectTransform scrollingArea;
        [SerializeField] [Min(0)] private float movementSpeed = 50f;

        private void Awake()
        {
            scrollingArea.anchoredPosition = new Vector2(scrollingArea.anchoredPosition.x , -scrollingArea.parent.GetComponent<RectTransform>().rect.height);
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = false;
        }

        private void Update()
        {
            float scroll_speed = (Input.GetButton("Submit") || Input.GetButton("Mouse Primary")) ? movementSpeed * 3 : movementSpeed;
            scrollingArea.transform.Translate(0, scroll_speed * Time.deltaTime, 0);

            if (scrollingArea.anchoredPosition.y > scrollingArea.rect.height)
                OnCreditsEnd();
        }

        public void OnCreditsEnd()
        {
            SceneLoader.Instance.LoadScene("Welcome", SceneLoader.TransitionType.FADE);
        }
    }
}