﻿using UnityEngine;
using UnityEngine.UI;

using Game;
using Game.Audio;
using Game.Scenes;

public class GameOverScene : MonoBehaviour
{
    [SerializeField] private Text scoreText;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        AudioManager.Instance.PlayMusic("Game Over", new AudioManager.MusicOptions(AudioManager.MusicTransition.NONE, false));
        scoreText.text = "Score : " + GameManager.Instance.Score.ToString("D4");
    }

    public void PlayAgainClick()
    {
        SceneLoader.Instance.LoadScene("Gameplay", SceneLoader.TransitionType.CIRCLE);
    }

    public void MainMenuClick()
    {
        SceneLoader.Instance.LoadScene("Welcome", SceneLoader.TransitionType.FADE);
    }
}