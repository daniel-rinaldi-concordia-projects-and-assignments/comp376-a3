﻿using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

using Design_Patterns;

namespace Game
{
    public class GameManager : MBSingleton<GameManager>
    {
        public enum Mode { NORMAL, SPECIAL }
        public Mode GameMode { get; set; }

        public int CurrentLevel { get; set; }
        public int Score { get; set; }

        protected override void Awake()
        {
            base.Awake();
            GameMode = Mode.NORMAL;
            CurrentLevel = -1;
            Score = 0;
        }

        private void Start()
        {
            #if UNITY_EDITOR
                string previous_scene = EditorPrefs.GetString("AutoLoadMainScene.PreviousScene");
                if (!string.IsNullOrEmpty(previous_scene) && previous_scene != EditorPrefs.GetString("AutoLoadMainScene.MainScene"))
                    EditorSceneManager.LoadScene(previous_scene);
            #endif

            // load first Scene
            if (!Application.isEditor)
                SceneManager.LoadScene("Splash");
        }

        protected override void OnApplicationQuit()
        {
            base.OnApplicationQuit();
        }
    }
}
