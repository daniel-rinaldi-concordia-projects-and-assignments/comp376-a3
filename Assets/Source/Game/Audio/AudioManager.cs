﻿using UnityEngine;
using UnityEngine.Audio;

using System.Collections;
using System.Collections.Generic;

using Design_Patterns;

namespace Game.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : MBSingleton<AudioManager>
    {
        [SerializeField] private AudioMixer masterMixer;
        [SerializeField] private AudioMixerGroup masterGroup;
        [SerializeField] private AudioMixerGroup musicGroup;
        [SerializeField] private AudioMixerGroup soundGroup;

        private Dictionary<string, int> music_dictionary;
        [Tooltip("List of music audio clips")]
        [SerializeField] private List<AudioClip> musicList;

        public enum MusicTransition { NONE, FADE }

        public class MusicOptions
        {
            public bool loop;
            public MusicTransition music_transition;
            public MusicOptions(MusicTransition music_transition = MusicTransition.NONE, bool loop = true)
            {
                this.loop = loop;
                this.music_transition = music_transition;
            }
        }

        private AudioSource source;

        protected override void Awake()
        {
            base.Awake();

            music_dictionary = new Dictionary<string, int>();

            if (musicList == null) musicList = new List<AudioClip>();

            // link dictionary entries (because unity does not support dictionaries as Serialized fields in the editor yet, lame...)
            for (int i = 0; i < musicList.Count; i++)
                music_dictionary.Add(musicList[i].name, i);

            source = this.GetComponent<AudioSource>();
            source.priority = 0;
        }

        public AudioClip GetMusic(string name)
        {
            if (music_dictionary.ContainsKey(name))
                return musicList[music_dictionary[name]];
            else
            {
                Debug.LogError("[AudioManager] : Music with name '" + name + "' not found in music list.");
                return null;
            }
        }

        public void PlayMusic(string name, MusicOptions options = null, AudioSource audio_source = null)
        {
            Play(GetMusic(name), (options != null ? options : new MusicOptions()), musicGroup, audio_source);
        }
        public void Play(AudioClip clip, MusicOptions options = null, AudioMixerGroup output = null, AudioSource audio_source = null)
        {
            if (options == null) options = new MusicOptions();
            if (audio_source == null) audio_source = source;

            if (audio_source.clip != null && audio_source.clip.name == clip.name) return;

            if (output != null) audio_source.outputAudioMixerGroup = output;

            // set source options
            source.loop = options.loop;

            float volume = audio_source.volume;

            if (options.music_transition == MusicTransition.FADE)
                StartCoroutine(MusicFadeTransition(audio_source, clip, volume));
            else
            {
                Stop(audio_source);
                audio_source.clip = clip;
                audio_source.Play();
            }
        }

        public void Stop(AudioSource audio_source = null)
        {
            if (audio_source == null) audio_source = source;
            audio_source?.Stop();
        }

        public void SetMasterVolume(float volume)
        {
            masterMixer.SetFloat("MasterVolume", volume);
        }

        public void SetMusicVolume(float volume)
        {
            masterMixer.SetFloat("MusicVolume", volume);
        }

        public void SetSoundVolume(float volume)
        {
            masterMixer.SetFloat("SoundVolume", volume);
        }

        private IEnumerator MusicFadeTransition(AudioSource audio_source, AudioClip clip, float max_volume)
        {
            while (audio_source.volume > 0f)
            {
                audio_source.volume -= 0.1f;
                yield return new WaitForSeconds(0.05f);
            }

            audio_source.Stop();
            audio_source.clip = clip;
            audio_source.Play();

            while (audio_source.volume < max_volume)
            {
                audio_source.volume += 0.1f;
                yield return new WaitForSeconds(0.05f);
            }
        }
    }
}