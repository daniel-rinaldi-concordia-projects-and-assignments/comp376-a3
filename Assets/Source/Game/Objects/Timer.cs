﻿using System.Collections.Generic;

using Design_Patterns;
using UnityEngine;
namespace Game.Objects
{
    public class Timer : Subject
    {
        protected List<Observer> observer_list;

        public string Name { get; set; }
        public float CurrentTime { get; set; }
        public float Time { get; set; }
        public bool Loop { get; set; }
        public bool IsActive { get; set; }

        public Timer(string name, float time, bool loop = false, bool is_active = false)
        {
            this.Name = name;
            this.Time = time >= 0 ? time : 0;
            this.Loop = loop;
            this.IsActive = is_active;

            observer_list = new List<Observer>();
            CurrentTime = this.Time;
        }

        public void StartTimer()
        {
            IsActive = true;
        }

        public void UpdateTimer(float time_step)
        {
            if (IsActive)
            {
                if (CurrentTime > 0f)
                    CurrentTime -= time_step;

                if (CurrentTime <= 0f)
                {
                    CurrentTime = 0f;
                    NotifyObservers();
                    ResetTimer();
                    IsActive = Loop;
                }
            }
        }

        public void ResetTimer()
        {
            CurrentTime = Time;
        }

        public void RegisterObserver(Observer observer)
        {
            observer_list.Add(observer);
        }

        public void UnregisterObserver(Observer observer)
        {
            observer_list.Remove(observer);
        }

        public void NotifyObservers()
        {
            foreach (Observer observer in observer_list)
                observer.Notify(this);
        }
    }
}
