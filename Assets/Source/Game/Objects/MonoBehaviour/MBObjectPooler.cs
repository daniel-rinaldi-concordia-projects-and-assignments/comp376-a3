﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Design_Patterns;

namespace Game.Objects
{
    /// <summary>
    /// A monobehaviour object pool.
    /// </summary>
    public class MBObjectPooler : MBSingleton<MBObjectPooler>
    {
        [System.Serializable]
        public class PoolDescriptor
        {
            public string name;
            public GameObject prefab;
            public int size;
        }

        [Tooltip("The default parent object for spawned objects. If null, then objects will be spawned as a child of this object.")]
        [SerializeField] protected GameObject defaultSpawnParent;

        [SerializeField] protected PoolDescriptor[] poolDescriptors;
        // This Dictionary's values are Tuples that contain the following: (Item1:"object pool queue", "index of PoolDescriptor related to the objects in Item1's queue")
        protected Dictionary<string, Tuple<Queue<GameObject>, int>> pool_dictionary;

        protected override void Awake()
        {
            base.Awake();

            pool_dictionary = new Dictionary<string, Tuple<Queue<GameObject>, int>>();
            
            for (int i = 0; i < poolDescriptors.Length; i++)
            {
                if (!pool_dictionary.ContainsKey(poolDescriptors[i].name))
                {
                    Tuple<Queue<GameObject>, int> object_pool_tuple = new Tuple<Queue<GameObject>, int>(new Queue<GameObject>(), i);
                    for (int j = 0; j < poolDescriptors[i].size; j++)
                    {
                        GameObject obj = Instantiate(poolDescriptors[i].prefab, this.gameObject.transform);
                        obj.SetActive(false);
                        object_pool_tuple.Item1.Enqueue(obj);
                    }

                    pool_dictionary.Add(poolDescriptors[i].name, object_pool_tuple);
                }
            }
        }

        public void ResizePool(string pool_name, int size)
        {
            if (!pool_dictionary.ContainsKey(pool_name))
            {
                Debug.LogError("Pool with name '" + pool_name + "' doesn't exist.");
                return;
            }

            Tuple<Queue<GameObject>, int> object_pool_tuple = pool_dictionary[pool_name];

            if (poolDescriptors[object_pool_tuple.Item2].size == size) return;

            if (size <= object_pool_tuple.Item1.Count)
            {
                for (int i = 0; i < size; i++)
                    Destroy(object_pool_tuple.Item1.Dequeue());
            }
            else
            {
                int extra = size - object_pool_tuple.Item1.Count;
                for (int i = 0; i < extra; i++)
                {
                    GameObject obj = Instantiate(poolDescriptors[object_pool_tuple.Item2].prefab);
                    obj.SetActive(false);
                    object_pool_tuple.Item1.Enqueue(obj);
                }
            }
            poolDescriptors[object_pool_tuple.Item2].size = size;
        }

        public GameObject Spawn(string pool_name, Vector3 position, Quaternion rotation, GameObject parent = null)
        {
            if (rotation == null) rotation = Quaternion.identity;
            if (parent == null) parent = defaultSpawnParent;
            if (parent == null) parent = this.gameObject;

            if (!pool_dictionary.ContainsKey(pool_name))
            {
                Debug.LogError("Pool with name '" + pool_name + "' doesn't exist.");
                return null;
            }

            GameObject obj = pool_dictionary[pool_name].Item1.Dequeue();
            obj.transform.parent = parent.transform;
            obj.SetActive(true);
            obj.transform.position = parent.transform.position + position;
            obj.transform.rotation = rotation;

            IPooledObject pooled_obj = obj.GetComponent<IPooledObject>();
            if (pooled_obj != null)
                pooled_obj.OnSpawn();

            pool_dictionary[pool_name].Item1.Enqueue(obj);

            return obj;
        }

        public void Despawn(GameObject game_object)
        {
            game_object.SetActive(false);
        }
    }

    /// <summary>
    /// An interface for pooled objects.
    /// </summary>
    public interface IPooledObject
    {
        /// <summary>
        /// This function is called when Object is spawned via ObjectPooler.Spawn()
        /// See <see cref="MBObjectPooler.Spawn()"/>
        /// </summary>
        void OnSpawn();
    }
}