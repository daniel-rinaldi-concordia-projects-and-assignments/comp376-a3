﻿using System.Collections;
using UnityEngine;

public class CausticsProjector : MonoBehaviour
{
    [SerializeField] private Texture2D[] frames;

    private Projector projector;

    private void Awake()
    {
        projector = GetComponent<Projector>();
    }

    private void OnEnable()
    {
        StartCoroutine(StartCaustics());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator StartCaustics()
    {
        int frame_index = 0;

        while (true)
        {
            projector.material.SetTexture("_ShadowTex", frames[frame_index]);
            frame_index = (frame_index + 1) % frames.Length;

            yield return new WaitForSeconds(0.05f);
        }
    }
}
