﻿using UnityEngine;

using Design_Patterns;

namespace Game.Objects
{
    public class PowerupSpawner : MonoBehaviour, Observer
    {
        [SerializeField] private GameObject objectOnDisplay;
        [SerializeField] private float powerupTime = 30f;
        private Timer powerup_timer;

        private AudioSource source;

        private void Awake()
        {
            if (GameManager.Instance.GameMode == GameManager.Mode.NORMAL)
                this.gameObject.SetActive(false);

            powerup_timer = new Timer("PowerupTimer", powerupTime, true, true);
            powerup_timer.RegisterObserver(this);
            source = this.GetComponent<AudioSource>();
        }

        private void Update()
        {
            powerup_timer.UpdateTimer(Time.deltaTime);
        }

        public void TakePowerup()
        {
            objectOnDisplay.SetActive(false);
            powerup_timer.ResetTimer();
            source.Play();
        }

        public void Notify(Subject subject)
        {
            if (subject is Timer && ((Timer)subject).Name == "PowerupTimer")
            {
                objectOnDisplay.SetActive(true);
            }
        }
    }
}