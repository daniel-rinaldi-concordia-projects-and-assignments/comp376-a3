﻿using UnityEngine;

using System.Collections.Generic;

namespace Game.Objects
{
    public class SharkSpawner : MonoBehaviour
    {
        [SerializeField] private Transform spawnPosition;

        [SerializeField] private GameObject sharkContainer;

        [SerializeField] private GameObject sharkBaitParent;

        [SerializeField] private List<GameObject> sharkPathParents;

        private int number_of_sharks_in_use;
        private int current_level;

        private void Awake()
        {
            if (sharkPathParents == null) sharkPathParents = new List<GameObject>();
            number_of_sharks_in_use = 3;
        }

        private void Start()
        {
            current_level = GameManager.Instance.CurrentLevel;

            MBObjectPooler.Instance.ResizePool("Shark", 10);
            for (int i = 0; i < number_of_sharks_in_use; i++)
                SpawnShark(i);
        }

        private void Update()
        {
            if (current_level != GameManager.Instance.CurrentLevel)
            {
                if (number_of_sharks_in_use < sharkPathParents.Count)
                    SpawnShark(number_of_sharks_in_use++);

                current_level = GameManager.Instance.CurrentLevel;
            }
        }

        public GameObject SpawnShark(int path_index)
        {
            GameObject shark_obj = MBObjectPooler.Instance.Spawn(
                "Shark",
                spawnPosition.position,
                Quaternion.identity,
                sharkContainer
            );

            Shark shark = shark_obj.GetComponent<Shark>();

            shark.sharkBaitParent = sharkBaitParent;
            shark.SetPath(sharkPathParents[path_index].transform);
            shark.Resize();

            return shark_obj;
        }
    }
}
