﻿using UnityEngine;

namespace Game.Objects
{
    public class Gold : MonoBehaviour
    {
        public enum GoldType { NONE, CRYSTAL, GOLD_BAR, MONEY_BAG };
        [SerializeField] private GoldType type;

        public int PointValue 
        {
            get
            {
                if (type == GoldType.MONEY_BAG)
                    return 1;
                else if (type == GoldType.CRYSTAL)
                    return 3;
                else if (type == GoldType.GOLD_BAR)
                    return 5;
                else
                    return 0;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "Gold Sweeper")
            {
                Destroy(this.gameObject);
            }
        }
    }
}
