﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace Game.Objects
{
    public class Spider : MonoBehaviour
    {
        [SerializeField] private Transform movementTransform;
        [SerializeField] private float targetDetectionDistance;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform shootingStartPosition;
        [SerializeField] private float moveSpeed = 10f;

        private Transform target;
        private Vector3 starting_position;
        private Vector3 path_end_position;
        private List<GameObject> shots_fired;
        private bool moving_outwards;
        private bool is_despawning;

        private void Awake()
        {
            target = Camera.main.transform;
            shots_fired = new List<GameObject>(15);
            starting_position = movementTransform.position;
            path_end_position = new Vector3(starting_position.x, starting_position.y, starting_position.z + 300f);
            moving_outwards = true;
        }

        private void Start()
        {
            StartCoroutine(ShootTarget());
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        private void Update()
        {
            shots_fired.RemoveAll(gameobject => gameobject == null);

            if (movementTransform.position == path_end_position)
                moving_outwards = false;

            if (moving_outwards)
            {
                movementTransform.position = Vector3.MoveTowards(movementTransform.position, path_end_position, moveSpeed * Time.deltaTime);
                movementTransform.LookAt(path_end_position, Vector3.up);
            }
            else
            {
                movementTransform.position = Vector3.MoveTowards(movementTransform.position, starting_position, moveSpeed * Time.deltaTime);
                movementTransform.LookAt(starting_position, Vector3.up);

                if (!is_despawning && movementTransform.position == starting_position)
                    Despawn();
            }
        }

        private IEnumerator ShootTarget()
        {
            while (true)
            {
                if (shots_fired.Count < 15)
                {
                    Vector3 self_to_target_vec = (target.position - shootingStartPosition.position);
                    if (self_to_target_vec.magnitude < targetDetectionDistance)
                        ShootTarget(self_to_target_vec.normalized);
                }

                yield return new WaitForSeconds(1);
            }
        }

        private void ShootTarget(Vector3 direction)
        {
            GameObject bullet_obj = Instantiate(bulletPrefab, shootingStartPosition.position, Quaternion.LookRotation(direction, movementTransform.up));

            bullet_obj.GetComponent<SpiderBullet>().Direction = direction;
            bullet_obj.GetComponent<SpiderBullet>().Speed = 8;

            Destroy(bullet_obj, 8f);

            shots_fired.Add(bullet_obj);
        }

        public void Despawn()
        {
            is_despawning = true;

            Destroy(movementTransform.gameObject);
        }
    }
}
