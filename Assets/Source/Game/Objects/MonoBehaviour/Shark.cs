﻿using UnityEngine;

namespace Game.Objects
{
    public class Shark : MonoBehaviour
    {
        [SerializeField] private float moveSpeed = 7f;
        [Min(0.1f)] [SerializeField] private float maxSharkScale;
        [Min(0.1f)] [SerializeField] private float minSharkScale;

        [SerializeField] private float detectionRange = 10f;
        [SerializeField] private float detectionRadius = 10f;

        public GameObject sharkBaitParent;
        [SerializeField] private GameObject nose;

        private Transform path;

        private AudioSource sound_effect;
        private int next_point_in_path;
        public float Speed { get; set; }

        private void Awake()
        {
            sound_effect = this.GetComponent<AudioSource>();
            next_point_in_path = 0;
            Speed = moveSpeed;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawLine(nose.transform.position, nose.transform.position + nose.transform.forward * detectionRange);
            Gizmos.DrawWireSphere(nose.transform.position + nose.transform.forward * detectionRange, detectionRadius);
        }

        private void Update()
        {
            Vector3 bait_vector = Vector3.zero;
            bool bait_detected = false;
            if (sharkBaitParent != null && sharkBaitParent.transform.childCount > 0)
            {
                for (int i = 0; i < sharkBaitParent.transform.childCount; i++)
                {
                    if (sharkBaitParent.transform.GetChild(i).gameObject.activeSelf && sharkBaitParent.transform.GetChild(i).position.y < 1f)
                    {
                        Vector3 temp_bait_vector = sharkBaitParent.transform.GetChild(i).position - nose.transform.position;

                        if (!bait_detected)
                            bait_vector = temp_bait_vector;
                        else if (bait_detected && temp_bait_vector.magnitude < bait_vector.magnitude)
                            bait_vector = sharkBaitParent.transform.GetChild(i).position - nose.transform.position;

                        bait_detected = true;
                    }
                }
            }

            bool player_detected = false;
            Vector3 next_point = this.transform.position;

            if (!bait_detected)
            {
                RaycastHit[] hits = Physics.SphereCastAll(nose.transform.position, detectionRadius, nose.transform.forward, detectionRange);
                if (hits.Length > 0)
                {
                    foreach (RaycastHit hit in hits)
                    {
                        if (hit.transform.tag == "Player" && hit.transform.position.y < 1f)
                        {
                            player_detected = true;
                            next_point = hit.transform.position;
                            break;
                        }
                    }
                }
            }

            if ((path != null && path.childCount > 0) || player_detected || bait_detected)
            {
                if (!player_detected && !bait_detected)
                    next_point = path.GetChild(next_point_in_path).transform.position;
                else if (bait_detected)
                    next_point = nose.transform.position + bait_vector;

                if (this.transform.position != next_point)
                {
                    this.transform.position = Vector3.MoveTowards(this.transform.position, next_point, Speed * Time.deltaTime);
                    this.transform.rotation = Quaternion.Slerp(
                        this.transform.rotation,
                        Quaternion.LookRotation(next_point - this.transform.position, Vector3.up),
                        (player_detected) ? Time.deltaTime * Time.deltaTime : Time.deltaTime
                    );
                }
                else
                    next_point_in_path = (next_point_in_path + 1) % path.childCount;
            }
        }

        public void Resize()
        {
            int rand = Random.Range((int)(minSharkScale * 10), (int)(maxSharkScale * 10));
            float shark_scale = rand / 10f;
            Resize(shark_scale);
        }
        public void Resize(float scale)
        {
            this.transform.localScale = new Vector3(scale, scale, scale);
            Speed = moveSpeed / scale;
        }

        public void PlaySoundEffect()
        {
            sound_effect.Play();
        }

        public void SetPath(Transform path_parent)
        {
            path = path_parent;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Shark Bait")
            {
                MBObjectPooler.Instance.Despawn(other.gameObject);
            }
        }
    }
}
