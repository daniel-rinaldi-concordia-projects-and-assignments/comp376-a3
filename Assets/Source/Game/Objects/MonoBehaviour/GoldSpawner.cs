﻿using UnityEngine;

using Design_Patterns;

namespace Game.Objects
{
    public class GoldSpawner : MonoBehaviour, Observer
    {
        [SerializeField] private GameObject[] goldPrefabs;
        [SerializeField] private int maxGoldToSpawn = 150;
        [SerializeField] private float goldSpawnTime = 3;
        [SerializeField] private GameObject spawnArea;

        private int gold_spawned;
        private Timer gold_timer;

        private void Awake()
        {
            gold_timer = new Timer("GoldTimer", goldSpawnTime, true, true);
            gold_timer.RegisterObserver(this);
            gold_spawned = 0;
        }

        private void Update()
        {
            gold_timer.UpdateTimer(Time.deltaTime);

            gold_spawned = spawnArea.transform.childCount;
        }

        public void Notify(Subject subject)
        {
            if (gold_spawned < maxGoldToSpawn && subject is Timer && ((Timer)subject).Name == "GoldTimer")
            {
                SpawnGold();
            }
        }

        private GameObject SpawnGold()
        {
            float x_half_scale = spawnArea.GetComponent<BoxCollider>().bounds.size.x / 2;
            float y_half_scale = spawnArea.GetComponent<BoxCollider>().bounds.size.z / 2;

            Vector3 rand_position_offset = new Vector3(Random.Range(-x_half_scale, x_half_scale), 0, Random.Range(-y_half_scale, y_half_scale));

            GameObject gold_obj = Instantiate(goldPrefabs[Random.Range(0, goldPrefabs.Length)], this.transform.position + rand_position_offset, Quaternion.identity, spawnArea.transform);

            return gold_obj;
        }
    }
}