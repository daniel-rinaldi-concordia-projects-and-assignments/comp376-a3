﻿using UnityEngine;

using System.Collections.Generic;

using Design_Patterns;

namespace Game.Objects
{
    public class MBTimer : MonoBehaviour
    {
        [System.Serializable]
        public class TimerDescriptor
        {
            public string name;
            public float time = 0f;
            public bool loop = true;
            public bool autoStart = true;
            public List<GameObject> observerObjects;
        }

        [Tooltip("List of Timer Descriptors. Each descriptor in this list should have a unique name.")]
        [SerializeField] private List<TimerDescriptor> timerDescriptors;
        protected Dictionary<string, Timer> timer_dictionary;

        protected void Awake()
        {
            timer_dictionary = new Dictionary<string, Timer>();
            foreach (TimerDescriptor timer_descriptor in timerDescriptors)
            {
                Timer timer = new Timer(timer_descriptor.name, timer_descriptor.time, timer_descriptor.loop, timer_descriptor.autoStart);

                if (timer_descriptor.observerObjects == null) timer_descriptor.observerObjects = new List<GameObject>();
                foreach (GameObject observerObj in timer_descriptor.observerObjects)
                {
                    Observer[] observer_array = observerObj.GetComponents<Observer>();
                    for (int i = 0; i < observer_array.Length; i++)
                        if (observer_array[i] != null) timer.RegisterObserver(observer_array[i]);
                }

                timer_dictionary[timer.Name] = timer;
            }
        }

        protected void Update()
        {
            foreach (KeyValuePair<string, Timer> timer_entry in timer_dictionary)
            {
                timer_entry.Value.UpdateTimer(Time.deltaTime);
            }
        }
    }
}