﻿using UnityEngine;

namespace Game.Objects
{
    public class SpiderBullet : MonoBehaviour
    {
        public Vector3 Direction { get; set; }
        public float Speed { get; set; }

        private void Awake()
        {
            if (Speed <= 0) Speed = 8;
        }

        private void Update()
        {
            this.GetComponent<Rigidbody>().velocity = this.transform.forward * Speed;
        }
    }
}