﻿using UnityEngine;

using Design_Patterns;

namespace Game.Objects
{
    public class SpiderSpawner : MonoBehaviour, Observer
    {
        [SerializeField] private GameObject spiderPrefab;
        [SerializeField] private Transform spawnPosition;
        [SerializeField] private Transform spiderContainer;

        [SerializeField] [Min(30f)] private float minSpawnTime = 45f;
        [SerializeField] [Min(30f)] private float maxSpawnTime = 90f;
        private Timer spawn_timer;

        [SerializeField] private AudioSource spawnAudioSource;

        private bool check_spawn_timer;
        private bool sound_played;
        private int current_level;

        private void Awake()
        {
            spawn_timer = new Timer("SpiderTimer", Random.Range(minSpawnTime, maxSpawnTime), false, true);
            spawn_timer.RegisterObserver(this);
        }

        private void Update()
        {
            if (spawn_timer.CurrentTime > 0f)
            {
                spawn_timer.UpdateTimer(Time.deltaTime);

                if (spawn_timer.CurrentTime <= 10f && !sound_played)
                {
                    spawnAudioSource.Play();
                    sound_played = true;
                }
            }

            if (check_spawn_timer)
            {
                if (spiderContainer.childCount <= 0)
                {
                    spawn_timer = new Timer("SpiderTimer", Random.Range(minSpawnTime, maxSpawnTime), false, true);
                    spawn_timer.RegisterObserver(this);
                    check_spawn_timer = false;
                    sound_played = false;
                }
            }
        }

        public void Notify(Subject subject)
        {
            if (subject is Timer && ((Timer)subject).Name == "SpiderTimer" && spiderContainer.childCount <= 0)
            {
                SpawnSpider();
                check_spawn_timer = true;
            }
        }

        private GameObject SpawnSpider()
        {
            spawnAudioSource.Play();
            GameObject spider_obj = Instantiate(spiderPrefab, spawnPosition.position, spiderPrefab.transform.rotation, spiderContainer);

            return spider_obj;
        }
    }
}
