﻿using UnityEngine;

public class RadarCamera : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private GameObject radarSun;

    private bool previous_fog_enabled;

    private void Update()
    {
        this.transform.position = new Vector3(target.position.x, this.transform.position.y, target.position.z);
    }

    private void OnPreCull()
    {
        radarSun.SetActive(true);
    }

    private void OnPreRender()
    {
        previous_fog_enabled = RenderSettings.fog;
        RenderSettings.fog = false;
        radarSun.SetActive(true);
    }

    private void OnPostRender()
    {
        RenderSettings.fog = previous_fog_enabled;
        radarSun.SetActive(false);
    }
}
