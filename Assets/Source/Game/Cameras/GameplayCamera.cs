﻿using UnityEngine;

using Game.Player;

public class GameplayCamera : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity = 100f;
    [SerializeField] private Transform playerTransform;

    [SerializeField] private GameObject waterSurfaceTop;
    [SerializeField] private GameObject waterSurfaceBottom;

    [SerializeField] private Color underWaterFogColor = new Color(0.3083838f, 0.5010307f, 0.6603774f, 1);
    [SerializeField] private float underWaterFogDensity = 0.02f;

    [SerializeField] private PlayerController playerController;
    [SerializeField] private GameObject causticsContainer;
    [SerializeField] private GameObject bubblesObject;

    private float rotation_x;
    private bool is_above_sea_level;
    private float oxygen;

    private void Awake()
    {
        rotation_x = 0;
        is_above_sea_level = true;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Start()
    {
        oxygen = playerController.Oxygen;
    }

    private void Update()
    {
        // under water
        if (transform.position.y < waterSurfaceTop.transform.position.y)
        {
            waterSurfaceTop.SetActive(false);
            waterSurfaceBottom.SetActive(true);
            causticsContainer.SetActive(true);

            ReduceOxygen();
        }
        else // above water
        {
            waterSurfaceTop.SetActive(true);
            waterSurfaceBottom.SetActive(false);
            causticsContainer.SetActive(false);
            bubblesObject.GetComponent<ParticleSystem>().Stop();
            ReplenishOxygen();
        }

        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        rotation_x -= mouseY;
        rotation_x = Mathf.Clamp(rotation_x, -90f, 90f);

        this.transform.localRotation = Quaternion.Euler(rotation_x, 0, 0);
        playerTransform.Rotate(Vector3.up * mouseX);

        // underwater fog effect
        bool above_sea_level_check = this.transform.position.y >= (waterSurfaceTop.transform.position.y);
        if (above_sea_level_check != is_above_sea_level)
        {
            RenderSettings.fog = true;
            RenderSettings.fogColor = underWaterFogColor;
            RenderSettings.fogDensity = underWaterFogDensity;
            RenderSettings.fogMode = FogMode.Exponential;
        }
        else
            RenderSettings.fog = false;
    }

    private void ReduceOxygen()
    {
        if (oxygen > -10)
        {
            oxygen -= Time.deltaTime;

            if (oxygen >= 0)
            {
                if (playerController.Oxygen >= (int)oxygen)
                    playerController.Oxygen = (int)oxygen;
                else
                    oxygen = playerController.Oxygen;
            }
            else
                playerController.Oxygen = 0;

            if (oxygen <= 0 && !playerController.IsChoking)
                playerController.Choke();
        }
        else
        {
            playerController.Die();
            oxygen = playerController.maxOxygen;
        }
    }

    private void ReplenishOxygen()
    {
        if (oxygen > 0 && playerController.IsChoking)
            playerController.StopChoking();

        if (oxygen < 0)
            oxygen = 0;

        oxygen += (Time.deltaTime * 10);

        if (oxygen > playerController.maxOxygen)
            oxygen = playerController.maxOxygen;

        if ((float)playerController.Oxygen < oxygen)
            playerController.Oxygen = (int)oxygen;
        else
            oxygen = playerController.Oxygen;
    }
}
