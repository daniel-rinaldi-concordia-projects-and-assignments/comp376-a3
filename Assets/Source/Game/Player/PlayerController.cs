﻿using UnityEngine;
using UnityEngine.UI;

using Game.Objects;
using Game.Scenes;

using Design_Patterns;

namespace Game.Player
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour, Observer
    {
        [Tooltip("How fast the player can move")]
        [SerializeField] private float moveSpeed = 4;

        [Tooltip("How fast the player can swim")]
        [SerializeField] private float swimSpeed = 12;

        [Tooltip("How fast the player can turn")]
        [SerializeField] private float turnSpeed = 10;

        [Tooltip("The maximum angle at which the player can move look vertically (only applies above water)")]
        [SerializeField] private float verticalLookAngleLimit = 85f;

        [SerializeField] private float jumpHeight = 2f;

        [SerializeField] private float damageInvincibilityTime = 2f;
        private Timer damage_invincibility_timer;
        private bool damage_invincibility;

        [SerializeField] private ParticleSystem bubblesParticleSystem;

        private CharacterController character_controller;
        private Animator animator;
        private float gravity;
        private Vector3 starting_position;
        private Vector3 input_vector;
        private Vector3 movement_vector;
        private Vector3 velocity;
        private bool is_moving;
        private bool is_above_sea_level;
        private bool jump_pressed;
        private bool is_swimming_up;
        private int lives;
        private bool respawn;

        private bool throw_shark_bait_pressed;
        private int shark_bait_on_hand;
        [SerializeField] private GameObject sharkBaitParent;

        [SerializeField] private int maxHealth = 3;
        private int health;
        [SerializeField] public int maxOxygen = 145;
        private int oxygen;
        private int last_bubbles_value;
        private int gold_on_hand;
        private bool has_speed_boost;
        private Timer speed_boost_timer;
        private bool speed_boost_active;

        [SerializeField] private AudioSource goldPickupSFX;
        [SerializeField] private AudioSource oxygenBeepSFX;

        [Header("UI")]
        [SerializeField] private Text oxygenText;
        [SerializeField] private Text healthText;
        [SerializeField] private Text totalGoldText;
        [SerializeField] private Text livesText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text sharkBaitText;
        [SerializeField] private Image speedBoostImage;

        public int Health
        {
            get { return health; }
            set
            {
                health = value >= 0 ? value : 0;
                healthText.text = health.ToString();
                
                if (health == 0)
                    Die();
            }
        }

        public int Oxygen
        {
            get { return oxygen; }
            set
            {
                if ((last_bubbles_value - value) >= 15)
                {
                    last_bubbles_value = value;
                    if (!is_above_sea_level)
                        bubblesParticleSystem.Play();
                }
                else if ((value - last_bubbles_value) >= 15)
                    last_bubbles_value = value;

                if (oxygen > 15 && value == 15)
                    OxygenBeep();

                oxygen = value;
                oxygenText.text = oxygen.ToString();
            }
        }

        public bool IsChoking { get; set; }

        private void Awake()
        {
            character_controller = this.GetComponent<CharacterController>();
            animator = this.GetComponent<Animator>();

            input_vector = Vector3.zero;
            movement_vector = Vector3.zero;
            velocity = Vector3.zero;
            damage_invincibility_timer = new Timer("DamageInvincibilityTimer", damageInvincibilityTime, false, true);
            damage_invincibility_timer.RegisterObserver(this);

            lives = 2;
            livesText.text = lives.ToString();

            gravity = Physics.gravity.y;

            Health = maxHealth;
            Oxygen = maxOxygen;
            last_bubbles_value = Oxygen;

            gold_on_hand = 0;
            totalGoldText.text = gold_on_hand.ToString();

            shark_bait_on_hand = 0;
            speed_boost_timer = new Timer("SpeedTimer", 10f, false, true);
            speed_boost_timer.RegisterObserver(this);

            starting_position = this.transform.position;
        }

        private void Update()
        {
            if (damage_invincibility)
                damage_invincibility_timer.UpdateTimer(Time.deltaTime);

            if (this.transform.position.y > 0.25f)
                is_above_sea_level = true;
            else
                is_above_sea_level = false;

            HandleInput();
            Move();

            UpdateAnimator();

            if (respawn)
            {
                IsChoking = false;
                health = maxHealth;
                Oxygen = maxOxygen;
                gold_on_hand = 0;

                totalGoldText.text = gold_on_hand.ToString();
                healthText.text = health.ToString();
                oxygenText.text = Oxygen.ToString();

                this.transform.position = starting_position;
                respawn = false;
            }

            if (throw_shark_bait_pressed && shark_bait_on_hand > 0)
            {
                Camera cam = Camera.main;
                GameObject bait_obj = MBObjectPooler.Instance.Spawn("Shark Bait", cam.transform.position, Quaternion.identity, sharkBaitParent);
                bait_obj.GetComponent<Rigidbody>().AddForce(cam.transform.forward * 8, ForceMode.Impulse);
                shark_bait_on_hand--;
                sharkBaitText.text = shark_bait_on_hand.ToString();
            }

            if (speed_boost_active)
                speed_boost_timer.UpdateTimer(Time.deltaTime);
        }

        private void UpdateAnimator()
        {
            animator.SetBool("isChoking", IsChoking);
        }

        private void HandleInput()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            input_vector = new Vector3(horizontal, 0, vertical);

            is_moving = horizontal != 0 || vertical != 0;

            jump_pressed = Input.GetButton("Jump") && (character_controller.isGrounded || (!character_controller.isGrounded && is_swimming_up));

            is_swimming_up = Input.GetButton("Jump") && !is_above_sea_level;

            throw_shark_bait_pressed = Input.GetButtonDown("Mouse Primary");

            if (Input.GetKeyDown(KeyCode.Alpha1) && has_speed_boost)
            {
                if (!speed_boost_active)
                {
                    swimSpeed *= 1.5f;
                }

                has_speed_boost = false;
                speed_boost_timer.ResetTimer();
                speed_boost_timer.StartTimer();
                speed_boost_active = true;
                speedBoostImage.gameObject.SetActive(false);
            }
        }

        private void Move()
        {
            if (is_above_sea_level)
                WalkMove();
            else
                SwimMove();
        }

        private void WalkMove()
        {
            movement_vector = this.transform.right * input_vector.x + this.transform.forward * input_vector.z;

            // simulate gravity
            if (character_controller.isGrounded)
                velocity.y = gravity * Time.deltaTime;
            else
                velocity.y += gravity * Time.deltaTime;

            if (jump_pressed)
                velocity.y = Mathf.Sqrt(-2 * jumpHeight * gravity);

            character_controller.Move((movement_vector * moveSpeed * Time.deltaTime) + (velocity * Time.deltaTime));
        }

        private void SwimMove()
        {
            Camera cam = Camera.main;

            movement_vector = cam.transform.right * input_vector.x + cam.transform.forward * input_vector.z;

            if (is_swimming_up)
            {
                movement_vector.y = movement_vector.y >= 1f ? 1f : movement_vector.y + 1f;
            }

            // simulate underwater gravity
            velocity.y = velocity.y >= 0f ? velocity.y : velocity.y - (gravity * Time.deltaTime);

            // calculate weight modifier
            float weight_modifier = (1 - Mathf.Clamp(gold_on_hand * 0.01f, 0, 0.5f));

            character_controller.Move((movement_vector * weight_modifier * swimSpeed * Time.deltaTime) + (velocity * Time.deltaTime));
        }

        public void Choke()
        {
            IsChoking = true;
        }

        public void StopChoking()
        {
            IsChoking = false;
        }

        public void Die()
        {
            if (lives > 0)
            {
                lives -= 1;
                livesText.text = lives.ToString();
                damage_invincibility = false;
                respawn = true;
            }
            else
            {
                // game over
                SceneLoader.Instance.LoadScene("GameOver", SceneLoader.TransitionType.FADE);
            }
        }

        public void OxygenBeep()
        {
            if (!oxygenBeepSFX.isPlaying)
                oxygenBeepSFX.Play();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Gold" && (Health > 0))
            {
                Gold gold = other.gameObject.GetComponent<Gold>();
                goldPickupSFX?.Play();

                gold_on_hand += gold.PointValue;
                totalGoldText.text = gold_on_hand > 99 ? "100+" : gold_on_hand.ToString();

                Destroy(other.gameObject);
            }
            else if (other.gameObject.tag == "TreasureBox" && (Health > 0))
            {
                if (gold_on_hand > 0)
                {
                    goldPickupSFX?.Play();
                    GameManager.Instance.Score += gold_on_hand;
                    scoreText.text = GameManager.Instance.Score.ToString("D4");
                    gold_on_hand = 0;
                    totalGoldText.text = gold_on_hand.ToString();
                }
            }
            else if (other.gameObject.tag == "SpiderBullet")
            {
                if (Oxygen > 0)
                Oxygen -= 5;
            }
            else if (other.gameObject.tag == "Spider" && !damage_invincibility && (Health > 0) && !respawn)
            {
                TakeDamage(3);
            }
            else if (other.gameObject.tag == "PowerupSpawner")
            {
                other.gameObject.GetComponent<PowerupSpawner>().TakePowerup();
                has_speed_boost = true;
                speedBoostImage.gameObject.SetActive(true);
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.tag == "Shark" && !damage_invincibility && (Health > 0))
            {
                other.gameObject.GetComponent<Shark>().PlaySoundEffect();

                TakeDamage(1);
            }
            else if (other.gameObject.tag == "Shark Bait Counter")
            {
                shark_bait_on_hand = 10;
                sharkBaitText.text = shark_bait_on_hand.ToString();
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Shark" && !damage_invincibility && (Health > 0))
            {
                collision.gameObject.GetComponent<Shark>().PlaySoundEffect();

                TakeDamage(1);
            }
        }

        public void TakeDamage(int damage)
        {
            if (!damage_invincibility)
            {
                damage_invincibility = true;
                damage_invincibility_timer.StartTimer();
                Health -= damage;
            }
        }

        public void Notify(Subject subject)
        {
            if (subject is Timer && ((Timer)subject).Name == "DamageInvincibilityTimer")
                damage_invincibility = false;
            else if (subject is Timer && ((Timer)subject).Name == "SpeedTimer")
            {
                speed_boost_active = false;
                swimSpeed /= 1.5f;
            }
        }
    }
}
