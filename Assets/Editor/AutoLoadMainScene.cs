﻿using UnityEngine;

using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public static class AutoLoadMainScene
{
    static AutoLoadMainScene()
    {
        // bind to OnPlayModeChanged callback
        EditorApplication.playModeStateChanged += OnPlayModeChanged;
    }

    // Menu items to select the Main scene and whether or not to load it
    [MenuItem("File/AutoLoadMainScene/Select Main Scene...")]
    private static void SelectMainScene()
    {
        string main_scene = EditorUtility.OpenFilePanel("Select Main Scene", Application.dataPath, "unity");
        main_scene = main_scene.Replace(Application.dataPath, "Assets");

        if (!string.IsNullOrEmpty(main_scene))
        {
            MainScene = main_scene;
            LoadMainSceneOnPlay = true;
        }
    }

    [MenuItem("File/AutoLoadMainScene/Load Main Scene On Play", true)]
    private static bool ShowLoadMainSceneOnPlay()
    {
        return !LoadMainSceneOnPlay;
    }
    [MenuItem("File/AutoLoadMainScene/Load Main Scene On Play")]
    private static void EnableLoadMainSceneOnPlay()
    {
        LoadMainSceneOnPlay = true;
    }

    [MenuItem("File/AutoLoadMainScene/Don't Load Main Scene On Play", true)]
    private static bool ShowDontLoadMainSceneOnPlay()
    {
        return LoadMainSceneOnPlay;
    }
    [MenuItem("File/AutoLoadMainScene/Don't Load Main Scene On Play")]
    private static void DisableLoadMainSceneOnPlay()
    {
        LoadMainSceneOnPlay = false;
    }

    // PlayModeChanged callback
    private static void OnPlayModeChanged(PlayModeStateChange state)
    {
        if (!LoadMainSceneOnPlay) return;

        if (!EditorApplication.isPlaying)
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                // User pressed play, autoload main scene but make sure user saves changes
                PreviousScene = EditorSceneManager.GetActiveScene().path;
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    try
                    {
                        EditorSceneManager.OpenScene(MainScene);
                    }
                    catch
                    {
                        Debug.LogError("Main Scene not found: " + MainScene);
                        EditorApplication.isPlaying = false;
                    }
                }
                else
                {
                    // User cancelled the save operation, cancel play as well
                    EditorApplication.isPlaying = false;
                }
            }

            // we were about to play, but user pressed stop, reload previous scene
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                try
                {
                    EditorSceneManager.OpenScene(PreviousScene);
                }
                catch
                {
                    Debug.LogError("Previous Scene not found: " + PreviousScene);
                }
            }
        }
    }

    // These properties will be set as editor preferences
    public const string EDITOR_PREFERENCE_LOAD_MAIN_SCENE_ON_PLAY = "AutoLoadMainScene.LoadMainSceneOnPlay";
    public const string EDITOR_PREFERENCE_MAIN_SCENE = "AutoLoadMainScene.MainScene";
    public const string EDITOR_PREFERENCE_PREVIOUS_SCENE = "AutoLoadMainScene.PreviousScene";

    private static bool LoadMainSceneOnPlay
    {
        get { return EditorPrefs.GetBool(EDITOR_PREFERENCE_LOAD_MAIN_SCENE_ON_PLAY, false); }
        set { EditorPrefs.SetBool(EDITOR_PREFERENCE_LOAD_MAIN_SCENE_ON_PLAY, value); }
    }

    private static string MainScene
    {
        get { return EditorPrefs.GetString(EDITOR_PREFERENCE_MAIN_SCENE, "Main.unity"); }
        set { EditorPrefs.SetString(EDITOR_PREFERENCE_MAIN_SCENE, value); }
    }

    private static string PreviousScene
    {
        get { return EditorPrefs.GetString(EDITOR_PREFERENCE_PREVIOUS_SCENE, EditorSceneManager.GetActiveScene().path); }
        set { EditorPrefs.SetString(EDITOR_PREFERENCE_PREVIOUS_SCENE, value); }
    }
}
